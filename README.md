# Indicaciones para ejecutar componente front para proyecto de Indicator-front

## Requisitos

Este proyecto fue generado con node v14.5.0, npm 6.14.5 y angular-cli 10.0.2


## Ejecución del proyecto

Para ejecutar el proyecto, se debe abrir una terminal y situarse dentro de la carpeta del proyecto.
Una vez dentro de la carpeta, ejecutar el comando "npm install" y luego "npm start", proceso que hará la compilación del proyecto y levantará un servidor local para poder visualizarlo. 
Este servidor local estará disponible a través del puerto por defecto, por lo que para visualizar el proyecto se debe ir al navegador web y abrir 
la siguiente url "http://localhost:4200", donde se podrá ver la pantalla principal de la aplicación.

##Notas Importantes

Para el correcto funcionamiento de la aplicación, se recomienda el uso de Google Chrome.

Para la muestra de datos desde la API desarrollada para este proyecto, se debe desplegar el componente WAR del proyecto en un servidor local y que escuche las peticiones por el puerto 8080.
