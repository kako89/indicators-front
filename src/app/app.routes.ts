import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { EvolutionComponent } from './components/evolution/evolution.component';



const APP_ROUTES: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'evolution/:key', component: EvolutionComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});