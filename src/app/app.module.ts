import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { RouterModule } from '@angular/router';



import { HttpClientModule } from '@angular/common/http';

import { APP_ROUTING } from './app.routes';
import { ChartsModule } from 'ng2-charts';

import { DatePipe } from '@angular/common';

import {IndicatorsService} from './services/indicators.service';

import { AppComponent } from './app.component';
import {HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { EvolutionComponent } from './components/evolution/evolution.component';
import { CardComponent } from './components/card/card.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    EvolutionComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    APP_ROUTING,
    ChartsModule
  ],
  providers: [
    IndicatorsService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
