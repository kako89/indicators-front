import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute} from '@angular/router'
import { Chart } from 'chart.js';
import { ChartsModule } from 'ng2-charts';
import { DatePipe } from '@angular/common';

import { IndicatorsService } from './../../services/indicators.service';

@Component({
  selector: 'app-evolution',
  templateUrl: './evolution.component.html',
  styleUrls: ['./evolution.component.css']
})
export class EvolutionComponent implements OnInit {


  values:any={};
  intervalUpdate: any = null;
  chart:any = null;
  name:string="";
  showChart:boolean = false;
  errorExist:boolean = false;
  loading:boolean = true;

  barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  barChartLabels = [];
  barChartType = 'line';
  barChartLegend = true;
  barChartData = [
    {data: [], label: 'Series A'}
  ];

  constructor(  private activatedRoute:ActivatedRoute,
                private indicatorsService:IndicatorsService,
                private datepipe:DatePipe) { 

    this.activatedRoute.params.subscribe(params => {
      this.values= this.getIndicatorEvolution(params['key']);
      this.name=params['key'];
    })


  }

  getIndicatorEvolution(key:string){
    this.values = this.indicatorsService.getIndicatorValues(key).subscribe(response=>{
      this.errorExist= false;
      this.barChartData[0]['data']= this.getValueSerie(response['values']);
      this.barChartData[0].label= 'Valores ' + key.toUpperCase();
      this.barChartLabels = this.getDateSerie(response['values']);
      this.loading = false;
      this.showChart = true;

    }, error=>{
      this.loading = false;
      this.errorExist= true;

    });
  }

  getDateSerie(data: any[]){
    let justDates:string[]=[];

    data.sort(((a, b) => a.date - b.date));

    for (let value of data) {
      let date = new Date(value['date'] * 1000);
      let dateToString = this.datepipe.transform(date, 'dd-MM-yyyy')
      justDates.push(dateToString);

    }
    

    return justDates;

  }

  getValueSerie(data: any[]){
    let justValues:number[]=[];

    data.sort(((a, b) => a.date - b.date));

    data.forEach(function (value) {
      justValues.push(value['value']);
    });

    return justValues;
  }


  ngOnInit(): void {  


  }






}
