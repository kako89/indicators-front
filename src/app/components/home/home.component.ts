import { Component } from '@angular/core';
import { IndicatorsService } from '../../services/indicators.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  
  indicators:any[]=[];
  errorExist:boolean=false;
  loading:boolean = true;

  constructor(private indicatorsService:IndicatorsService){
    this.obtenerIndicadores();
  }

  obtenerIndicadores(){
    this.indicatorsService.getIndicators().subscribe(response=>{
      this.errorExist=false;
      this.indicators=response;
      this.loading= false;

    }, error=>{
      this.errorExist=true;
      this.loading= false;
    }
    );
  }

}
