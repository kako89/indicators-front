import {Injectable} from '@angular/core'
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class IndicatorsService {


    constructor(private httpClient: HttpClient){
        
    }

    getApiUrl():string{
        return "http://localhost:8080/indicator/";
    }

    getIndicators(): Observable<any>{
        return this.httpClient.get(this.getApiUrl() + "last");
    }

    getIndicatorValues(key: string){
        return this.httpClient.get(this.getApiUrl()+"values?key="+key);
    }


}